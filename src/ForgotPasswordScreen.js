import {
  Button,
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';

const ForgotPasswordScreen = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
      <TouchableOpacity
        style={{alignSelf: 'flex-start'}}
        onPress={() => navigation.goBack()}>
        <Image
          style={style.backButton}
          source={require('../assets/back-button.png')}
        />
      </TouchableOpacity>

      <Image
        style={style.marlinIcon}
        source={require('../assets/marlin-icon.png')}
      />
      <Text style={style.titleText}>Reset your password</Text>
      <TextInput style={style.textInput} placeholder="Email" />

      <TouchableOpacity
        onPress={() => navigation.navigate('login')}
        style={style.loginButton}>
        <Text style={style.textLogin}>Request Reset</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ForgotPasswordScreen;

const style = StyleSheet.create({
  backButton: {
    marginLeft: 15,
    marginVertical: 30,
    alignSelf: 'flex-start',
  },

  marlinIcon: {
    marginTop: 50,
    marginBottom: 30,
    marginHorizontal: 30,
    height: 45,
  },

  titleText: {
    marginVertical: 20,
    color: '#5B5B5B',
    fontSize: 20,
  },
  textInput: {
    marginVertical: 10,
    marginHorizontal: 30,
    paddingHorizontal: 20,
    borderRadius: 10,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },

  loginButton: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  textLogin: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
