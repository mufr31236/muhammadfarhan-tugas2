import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from './HomeScreen';
import UserScreen from './UserScreen';
import HelpScreen from './HelpScreen';
import BookingScreen from './BookingScreen';
import TabBar from './TabBar';

const Tab = createBottomTabNavigator();

const BottomTabNavigation = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      tabBar={prop => <TabBar {...prop} />}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="My Booking" component={BookingScreen} />
      <Tab.Screen name="Help" component={HelpScreen} />
      <Tab.Screen name="User" component={UserScreen} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigation;
