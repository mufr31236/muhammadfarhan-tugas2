import {Image, StyleSheet, Text, View} from 'react-native';
import React, {Component} from 'react';

const HomeScreen = () => {
  return (
    <View style={style.container}>
      <Image
        style={style.banner}
        source={require('../assets/marlin_banner.png')}
      />
      <View style={style.menuContainer}>
        <Image
          style={style.menuIcon}
          source={require('../assets/ic_ferry_intl.png')}
        />
        <Image
          style={style.menuIcon}
          source={require('../assets/ic_ferry_domestic.png')}
        />
        <Image
          style={style.menuIcon}
          source={require('../assets/ic_attraction.png')}
        />
        <Image
          style={style.menuIcon}
          source={require('../assets/ic_pioneership.png')}
        />
      </View>
    </View>
  );
};

export default HomeScreen;

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F4F4F4',
  },

  banner: {
    width: '100%',
    height: 142,
    resizeMode: 'stretch',
    marginBottom: 10,
  },

  menuContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  menuIcon: {
    height: 72,
    width: 72,
    paddingHorizontal: 5,
  },
});
