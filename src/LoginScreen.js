import {
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';

const LoginScreen = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
      <Image
        style={style.marlinIcon}
        source={require('../assets/icon_marlin.png')}
      />
      <TextInput style={style.textInput} placeholder="Username" />

      <TextInput style={style.textInput} placeholder="********" />

      <TouchableOpacity
        onPress={() => navigation.navigate('home')}
        style={style.loginButton}>
        <Text style={style.textLogin}>Sign in</Text>
      </TouchableOpacity>

      <Text
        onPress={() => navigation.navigate('forgotPassword')}
        style={style.forgotPassword}>
        Forgot Password
      </Text>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 30,
        }}>
        <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
        <View>
          <Text
            style={{
              width: '100%',
              textAlign: 'center',
              fontSize: 12,
              color: '#2E3283',
              paddingHorizontal: 10,
            }}>
            Login With
          </Text>
        </View>
        <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
      </View>

      <View style={style.alternateLoginContainer}>
        <Image
          style={style.alternateIcon}
          source={require('../assets/icon_fb.png')}
        />
        <Image
          style={style.alternateIcon}
          source={require('../assets/icon_google.png')}
        />
      </View>

      <Text>App Version 2.8.3</Text>
      <View style={style.footer}>
        <Text style={{padding: 10}}>
          Don't have an account?
          <Text
            onPress={() => navigation.navigate('register')}
            style={style.innerFooterText}>
            Sign Up
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default LoginScreen;
const style = StyleSheet.create({
  marlinIcon: {
    marginTop: 100,
    marginBottom: 30,
    marginHorizontal: 50,
    paddingHorizontal: 10,
    height: 85,
  },

  textInput: {
    marginVertical: 10,
    marginHorizontal: 30,
    paddingHorizontal: 20,
    borderRadius: 10,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },

  loginButton: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  textLogin: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },

  forgotPassword: {
    marginHorizontal: 30,
    marginBottom: 20,
    fontSize: 12,
    color: '#2E3283',
  },

  alternateLoginContainer: {
    marginHorizontal: 30,
    marginVertical: 40,
    flexDirection: 'row',
  },

  alternateIcon: {
    height: 50,
    width: 50,
    marginHorizontal: 20,
  },

  appVersionText: {
    marginHorizontal: 30,
    marginBottom: 20,
    fontSize: 12,
    color: '#828282',
  },

  footer: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#FFFFFF',
    width: '100%',
    alignItems: 'center',
  },

  innerFooterText: {
    fontWeight: 'bold',
  },
});
