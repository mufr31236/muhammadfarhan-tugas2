import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';

const RegisterScreen = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
      <Image
        style={style.marlinIcon}
        source={require('../assets/icon_marlin.png')}
      />

      <TextInput style={style.textInput} placeholder="Name" />
      <TextInput style={style.textInput} placeholder="Email" />
      <TextInput style={style.textInput} placeholder="Phone" />
      <TextInput style={style.textInput} placeholder="Password" />

      <TouchableOpacity style={style.loginButton}>
        <Text style={style.textLogin}>Sign up</Text>
      </TouchableOpacity>

      <View style={style.footer}>
        <Text style={{padding: 10}}>
          Already have an account?
          <Text
            onPress={() => navigation.navigate('login')}
            style={style.innerFooterText}>
            Log in
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default RegisterScreen;

const style = StyleSheet.create({
  marlinIcon: {
    marginTop: 100,
    marginBottom: 30,
    marginHorizontal: 50,
    paddingHorizontal: 10,
    height: 85,
  },
  textInput: {
    marginVertical: 10,
    marginHorizontal: 30,
    paddingHorizontal: 20,
    borderRadius: 10,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },

  loginButton: {
    marginHorizontal: 30,
    marginVertical: 20,
    backgroundColor: '#2E3283',
    alignSelf: 'stretch',
    borderRadius: 10,
    alignContent: 'center',
  },

  textLogin: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },

  footer: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#FFFFFF',
    width: '100%',
    alignItems: 'center',
    shadowRadius: 2,
    shadowOpacity: 2,
  },

  innerFooterText: {
    fontWeight: 'bold',
  },
});
